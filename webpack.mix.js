let mix = require('laravel-mix');
require('@ayctor/laravel-mix-svg-sprite');
require('laravel-mix-react-css-modules');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
	* ADMIN
 */
//
// mix.js('resources/assets/js/admin/admin.js', 'public/assets/admin/js')
// 	.js('resources/assets/js/admin/send_progress.js', 'public/assets/admin/js');
//
// mix.sass('resources/assets/css/admin/admin.scss', 'public/assets/admin/css');
//

mix.react('resources/js/app.js', 'assets/js');
// mix.react('resources/react/js/app.js', 'public/react/js').reactCSSModules();
// .sass('resources/react/sass/app.scss', 'public/react/css');


mix.svgSprite('resources/assets/svg/*.svg', 'assets/svg/sprite.svg');

// mix.browserSync('localhost:8000');

mix.browserSync({
	proxy: 'localhost:8080',
	open: false,
	files: [
		'resources/views/**/*.blade.php',
		'public/assets/js/*.js',
		'public/assets/css/*.css',
		'public/assets/svg/sprite.svg',
		// 'public/assets/admin/js/*.js',
		// 'public/assets/admin/css/*.css'
	]
});
