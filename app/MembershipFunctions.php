<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipFunctions extends Model
{

    protected $table = 'membershipfunctions';

    protected $fillable = [
        'type',
        'par1',
        'par2',
        'par3',
        'par4'
    ];

}
