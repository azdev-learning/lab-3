<?php

namespace App\Http\Controllers;

use App\Car;
use App\FuzzyRules;
use App\FuzzyVariables;
use App\GlobalRule;
use App\LinguisticVariables;
use App\LocalRule;
use App\MembershipFunctions;
use App\Question;
use Illuminate\Http\Request;

class FuzzyController extends Controller
{

    public function jsonGetForm(Request $request)
    {
        $variables = LinguisticVariables::query()->where("id", "!=", "12")->get();
        return ["linguisticVariables" => json_encode($variables, JSON_PRETTY_PRINT)];
    }

    public function jsonCount(Request $request)
    {
        $data = $request->input("data");
        $fuzzyfications = array();
        foreach ($data as $id => $value) {
            $fuzzyfications[$id] = $this->fuzzyficate($id, $value);
        }

        $agregation = $this->agregation($fuzzyfications);

        $activation = $this->activation($fuzzyfications, $agregation);



        $accumulated = $this->accumulate($activation);

        $defuzzyficated = $this->defuzzyficate($accumulated, $activation["rule"]);


        return ["fuzzyfication" => json_encode($fuzzyfications, JSON_PRETTY_PRINT), "agregation" => json_encode($agregation, JSON_PRETTY_PRINT), "activation" => json_encode($activation, JSON_PRETTY_PRINT), "accumulated" => json_encode($accumulated, JSON_PRETTY_PRINT), "defuzzyficated" => $defuzzyficated];
    }

    public function fuzzyficate($id, $value)
    {
        $linguisticVariable = LinguisticVariables::find($id);
        $fuzzyVariables = FuzzyVariables::query()->where("linguistic_variable_id", $id)->get();

        $fuzzyfication = false;

        if ($fuzzyVariables->isNotEmpty()) {
            $results = array();
            $response = array(
                "name" => $linguisticVariable->name,
                "x" => $value,
                "id" => $linguisticVariable->id,
                "fuzzyVariables" => array()
            );

            foreach ($fuzzyVariables as $fuzzyVariable) {
                $membershipFunction = MembershipFunctions::query()->find($fuzzyVariable->membership_function_id);
                $y = $this->countFunction($membershipFunction, $value, $linguisticVariable->x_min, $linguisticVariable->x_max);
                $graphValues = $this->getGraphValues($membershipFunction, $linguisticVariable->x_min, $linguisticVariable->x_max);
                $results[$fuzzyVariable->id] = $y;
                $response["fuzzyVariables"][$fuzzyVariable->id] = array(
                    "y" => $y,
                    "graph" => $graphValues,
                    "name" => $fuzzyVariable->name,
                    "id" => $fuzzyVariable->id,
                    "rule" => $linguisticVariable->name
                );
            }

            $maxValue = max($results);
            $response["active"] = FuzzyVariables::query()->find(array_search($maxValue, $results))->name;
            $response["y"] = $maxValue;

            $fuzzyfication = $response;
        }

        return $fuzzyfication;
    }

    public function countFunction($membershipFunction, $x, $min, $max, $useMin = False, $limit = 0)
    {
        $y = match (trim($membershipFunction->type)) {
            "partial decrease" => $this->countPartialDecrease($membershipFunction->par1, $membershipFunction->par2, $x, $min, $useMin, $limit),
            "partial increase" => $this->countPartialIncrease($membershipFunction->par1, $membershipFunction->par2, $x, $max, $useMin, $limit),
            "trapezoidal" => $this->countTrapezoidal($membershipFunction->par1, $membershipFunction->par2, $membershipFunction->par3, $membershipFunction->par4, $x, $useMin, $limit),
            default => dd($membershipFunction)
        };

        return $y;
    }

    public function countPartialDecrease($par1, $par2, $x, $min, $useMin = False, $limit = 0)
    {
        $y = 0;
        if ($x < $par1 && $x >= $min) {
            $y = $useMin ? min([$limit, 1]) : 1;
        } elseif ($x >= $par1 && $x <= $par2) {
            $y = $useMin ? min([($par2 - $x) / ($par2 - $par1), $limit]) : ($par2 - $x) / ($par2 - $par1);
        } elseif ($x > $par2) {
            $y = 0;
        }

        return $y;
    }

    public function countPartialIncrease($par1, $par2, $x, $max, $useMin = False, $limit = 0)
    {
        $y = 0;
        if ($x < $par1) {
            $y = 0;
        } elseif ($x >= $par1 && $x <= $par2) {
            $y = $useMin ? min([$limit, ($x - $par1) / ($par2 - $par1)]) : ($x - $par1) / ($par2 - $par1);
        } elseif ($x > $par2 && $x <= $max) {
            $y = $useMin ? min([$limit, 1]) : 1;
        }

        return $y;
    }

    public function countTrapezoidal($par1, $par2, $par3, $par4, $x, $useMin = False, $limit = 0)
    {
        $y = 0;
        if ($x < $par1) {
            $y = 0;
        } elseif ($x >= $par1 && $x < $par2) {
            $y = $useMin ? min([$limit, ($x - $par1) / ($par2 - $par1)]) : ($x - $par1) / ($par2 - $par1);
        } elseif ($x >= $par2 && $x <= $par3) {
            $y = $useMin ? min([$limit, 1]) : 1;
        } elseif ($x >= $par3 && $x <= $par4) {
            $y = $useMin ? min([$limit, ($par4 - $x) / ($par4 - $par3)]) : ($par4 - $x) / ($par4 - $par3);
        } elseif ($x > $par4) {
            $y = 0;
        }

        return $y;
    }

    public function getGraphValues($function, $min, $max, $step = 50, $useMin = False, $limit = 0)
    {
        $dataset = array();
        for ($x = $min; $x <= $max; $x += $step) {
            $dataset[$x] = $this->countFunction($function, $x, $min, $max, $useMin, $limit);
        }
        return $dataset;
    }

    public function agregation($fuzzyfications)
    {
        $agregation = array();

        $param_1 = array_values($fuzzyfications)[0]["fuzzyVariables"];
        $param_2 = array_values($fuzzyfications)[1]["fuzzyVariables"];

//        for ($par1 = 0; $par1 < count($param_1); $par1++)
//        {
//            $agregation[$par1] = array();
//            for($par2 = 0; $par2 < count($param_2); $par2++)
//            {
//                $agregation[$par1][$par2] = min([
//                   array_values($param_1)[$par1]["y"],
//                    array_values($param_2)[$par2]["y"],
//                ]);
//            }
//        }
        foreach ($param_1 as $var_1) {
            foreach ($param_2 as $var_2)
            {
                $output = [
                    "text_1" => $var_1["rule"]." ".$var_1["name"],
                    "text_2" => $var_2["rule"]." ".$var_2["name"],
                    "id_1" => $var_1["id"],
                    "id_2" => $var_2["id"],
                    "value" => min([$var_1["y"], $var_2["y"]])
                ];
                array_push($agregation, $output);
            }
        }


        return $agregation;
    }

    public function activation($fuzzyfication, $agregation)
    {
        $activation = array();

        $rules = $this->getFuzzyRules($fuzzyfication, $agregation);

        if (count($rules) > 0) {
            $activation["graph"] = $this->getFuzzyRuleGraph($rules[0]["rule"]);
            $activation["rule"] = $rules[0]["rule"];
        }

        $activation["activated"] = [];

        foreach ($rules as $ruleObj) {
            $output = [];

            $rule = $ruleObj["rule"];
            $limit = $ruleObj["value"];

            $ling_1 = LinguisticVariables::query()->find($rule->if_var1);
            $var_1 = FuzzyVariables::query()->find($rule->if_val1);

            $ling_2 = LinguisticVariables::query()->find($rule->if_var2);
            $var_2 = FuzzyVariables::query()->find($rule->if_val2);

            $ling_3 = LinguisticVariables::query()->find($rule->then_var);
            $var_3 = FuzzyVariables::query()->find($rule->then_val);

            $operation = match ($rule->operation) {
                1 => "И",
                2 => "ИЛИ",
                default => "И"
            };

            $output["id"] = $rule->id;
            $output["name"] = $ling_3->name;

            $output["text"] = "ЕСЛИ " . $ling_1->name . " " . $var_1->name . " " . $operation . " " . $ling_2->name . " " . $var_2->name . " ТО " . $ling_3->name . " " . $var_3->name;

            $function = MembershipFunctions::query()->find($var_3->membership_function_id);

            $output["graph"] = $this->getGraphValues($function, $ling_3->x_min, $ling_3->x_max, 1, True, $limit);

            array_push($activation["activated"], $output);
        }

        return $activation;
    }

    public function getFuzzyRules($fuzzyfication, $agregation)
    {
        $rules = array();

        $fuzzyficationList = array_values($fuzzyfication);


        foreach ($agregation as $index)
        {
            if ($index["value"] > 0)
            {
                $fuzzyRule = new FuzzyRules();
                $rule = $fuzzyRule->findRule($fuzzyficationList[0]["id"], $index["id_1"], $fuzzyficationList[1]["id"], $index["id_2"]);
                array_push($rules, ["value" => $index["value"], "rule" => $rule]);
            }
        }

        return $rules;
    }

    public function getFuzzyRuleGraph($rule)
    {
        $linguisticVariable = LinguisticVariables::find($rule->then_var);
        $fuzzyVariables = FuzzyVariables::query()->where("linguistic_variable_id", $linguisticVariable->id)->get();

        $datasets = array();

        foreach ($fuzzyVariables as $fuzzyVariable) {
            $membershipFunction = MembershipFunctions::query()->find($fuzzyVariable->membership_function_id);
            $graphValues = $this->getGraphValues($membershipFunction, $linguisticVariable->x_min, $linguisticVariable->x_max, 1);
            $datasets[$fuzzyVariable->name] = $graphValues;
        }

        return $datasets;
    }

    public function accumulate($activated) {
        $graph = [];

        $graphs = [];

        foreach ($activated["activated"] as $obj)
        {
            array_push($graphs, $obj["graph"]);
        }


        for ($x = 0; $x < count($activated["activated"][0]["graph"]); $x++)
        {
            $values = [];
            foreach ($graphs as $dataset)
            {
                array_push($values, $dataset[$x]);
            }
            array_push($graph, max($values));
        }

        return $graph;
    }


    public function defuzzyficate($accumulated, $rule)
    {

        $numerator = 0;
        $steps = count($accumulated);

        for ($step = 0; $step < $steps; $step++)
        {
            $numerator += $step * $accumulated[$step];
        }

        $denominator = array_sum($accumulated);

        return $numerator / $denominator;
    }

}
