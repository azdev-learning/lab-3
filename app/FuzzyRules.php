<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuzzyRules extends Model
{

    protected $table = 'fuzzyrules';

    protected $fillable = [
        'if_var1',
        'if_val1',
        'operation',
        'if_var2',
        'if_val2',
        'then_var',
        'then_val',
        'confidence'
    ];

    public function findRule($lingVar1, $fuzzyVar1, $lingVar2, $fuzzyVar2) {
        return FuzzyRules::query()->where([
            ['if_var1', '=', $lingVar1],
            ['if_val1', '=', $fuzzyVar1],
            ['if_var2', '=', $lingVar2],
            ['if_val2', '=', $fuzzyVar2]
        ])->first();
    }


}
