<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuzzyVariables extends Model
{

    protected $table = 'fuzzyvariables';

    protected $fillable = [
        'name',
        'membership_function_id',
        'linguistic_variable_id'
    ];

    public function MembershipFunction () {
        return $this->hasOne(MembershipFunctions::class, 'id', 'membership_function_id');
    }
    public function LinguisticVariable () {
        return $this->hasOne(LinguisticVariables::class, 'id', 'linguistic_variable_id');
    }

}
