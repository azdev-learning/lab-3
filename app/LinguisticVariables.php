<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinguisticVariables extends Model
{

    protected $table = 'linguisticvariables';

    protected $fillable = [
        'name',
        'x_min',
        'x_max'
    ];

}
