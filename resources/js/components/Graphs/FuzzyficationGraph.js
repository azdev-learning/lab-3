import React from "react";
import annotation from 'chartjs-plugin-annotation';
import {Line} from 'react-chartjs-2';


const colors = [
    '#FFABAB',
    '#91D5FF',
    '#FFFD78',
    '#13678A',
    '#012030'
]

Chart.plugins.register({
    annotation
});

export const FuzzyficationGraph = ({fuzzyficationResults}) => {
    const options = {
        responsive: true,
        title: {
            display: true,
            text: fuzzyficationResults.name,
        },
        annotation: {
            annotations: [{
                drawTime: 'afterDraw', // overrides annotation.drawTime if set
                id: 'selectedValue', // optional
                type: 'line',
                mode: 'vertical',
                scaleID: 'x-axis-0',
                value: fuzzyficationResults.x.toString(),
                borderColor: 'red',
                borderWidth: 3,
            }]
        }

    };
    const fuzzyVariables = fuzzyficationResults.fuzzyVariables;
    const fuzzyIds = Object.keys(fuzzyVariables);
    const labels = Object.keys(fuzzyVariables[fuzzyIds[0]].graph);
    const datasets = fuzzyIds.map((key, index) => {
        const variable = fuzzyVariables[key];
        const dataset = {
            label: variable.name || `Id ${key}`,
            data: Object.values(variable.graph),
            borderColor: colors[index],
            backgroundColor: 'rgba(0,0,0,0)'
        };
        return dataset;
    });

    const data = {
        labels,
        datasets
    };
    return (
        Object.keys(fuzzyficationResults).length > 0 ?
            <div>
                <Line key={fuzzyficationResults.x} options={options} data={data} type={"line"}/>
                <span>
                    y(x = {fuzzyficationResults.x}) = {fuzzyficationResults.y}
                    <br/>
                    {`${fuzzyficationResults.name} ${fuzzyficationResults.active}`}
                </span>
            </div>
            :
            <div className={"placeholder"}/>
    )
}
