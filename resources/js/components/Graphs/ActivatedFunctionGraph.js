import React from "react";
import annotation from 'chartjs-plugin-annotation';
import {Line} from 'react-chartjs-2';


const colors = [
    '#FFABAB',
    '#91D5FF',
    '#FFFD78',
    '#13678A',
    '#012030'
];

export const ActivatedFunctionGraph = ({activationResults}) => {
    const options = {
        responsive: true,
    };

    const labels = [...Array(activationResults[Object.keys(activationResults)[0]].length).keys()];
    const datasets = Object.keys(activationResults).map((key, index) => {
        const dataset = {
            label: key,
            data: activationResults[key],
            borderColor: colors[index],
            backgroundColor: 'rgba(0,0,0,0)'
        };
        return dataset;
    });

    console.log(labels, datasets);

    const data = {
        labels,
        datasets
    };
    return (
        Object.keys(activationResults).length > 0 ?
            <div className={"basegraph"}>
                <Line key={Object.keys(activationResults)[0]} options={options} data={data} type={"line"}/>
            </div>
            :
            <div className={"placeholder"}/>
    )
}
