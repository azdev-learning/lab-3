import React from "react";
import {Line} from 'react-chartjs-2';


const colors = [
    '#FFABAB',
    '#91D5FF',
    '#FFFD78',
    '#13678A',
    '#012030'
];

export const ActivatedGraph = ({graph, text, label}) => {
    const options = {
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 0.1,
                    max: 1
                }
            }]
        },
    };

    const labels = [...Array(graph?.length).keys()];

    const datasets = [
        {
            label: label,
            data: graph,
            borderColor: colors[0],
            backgroundColor: 'rgba(0,0,0,0)'
        }
    ]

    const data = {
        labels,
        datasets
    };
    return (
        graph.length > 0 ?
            <div>
                <Line key={text} options={options} data={data} type={"line"}/>
                <p>{text}</p>
            </div>
            :
            <div className={"placeholder"}/>
    )
}
