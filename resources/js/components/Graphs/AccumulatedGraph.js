import React from "react";
import {Line} from 'react-chartjs-2';


const colors = [
    '#FFABAB',
    '#91D5FF',
    '#FFFD78',
    '#13678A',
    '#012030'
];

export const AccumulatedGraph = ({graph, baseGraph}) => {
    const options = {
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 0.1,
                    max: 1
                }
            }]
        },
    };

    console.log(graph, baseGraph);

    const labels = [...Array(baseGraph[Object.keys(baseGraph)[0]].length).keys()];

    const datasets = Object.keys(baseGraph).map((key, index) => {
        return {
            label: key,
            data: baseGraph[key],
            borderColor: colors[index],
            backgroundColor: 'rgba(0,0,0,0)'
        };
    });

    datasets.unshift({
        label: "Аккумулированое значение",
        data: graph,
        borderColor: "red",
        backgroundColor: 'rgba(0,0,0,0)'
    })

    const data = {
        labels,
        datasets
    };
    return (
        graph.length > 0 ?
            <div className={"accumulated"}>
                <Line key={graph[0]} options={options} data={data} type={"line"}/>
            </div>
            :
            <div className={"placeholder"}/>
    )
}
