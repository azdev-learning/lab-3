import React from "react";
import axios from "axios";
import {AntdSlider} from "../IconSlider/IconSlider";
import {Button} from 'antd';
import "./form.scss";
import {FuzzyficationGraph} from "../Graphs/FuzzyficationGraph";
import {ActivatedFunctionGraph} from "../Graphs/ActivatedFunctionGraph";
import {ActivatedGraph} from "../Graphs/ActivatedGraph";
import {AccumulatedGraph} from "../Graphs/AccumulatedGraph";


export class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formFields: [],
            formResults: {},
            fuzzyfication: {},
            activation: {},
            accumulated: {},
            defuzzyficated: undefined
        }
    }

    componentDidMount() {
        axios.post('/getVariablesForm.json', {_token: this.token})
            .then(response => {
                const {linguisticVariables} = response.data;
                const formResults = {};
                const variablesArr = JSON.parse(linguisticVariables);
                variablesArr.forEach((variable) => {
                    formResults[variable.id] = variable.x_min;
                });

                this.setState({formFields: variablesArr, formResults});
            })
    }

    onSliderChange(id, value) {
        const arr = this.state.formResults;
        arr[id] = value;
        this.setState({formResults: arr});
    }

    send() {
        console.log(this.state.formResults);
        axios.post('/fuzzyficate.json', {_token: this.token, data: this.state.formResults})
            .then(response => {
                console.log(response);
                const fuzzyfication = JSON.parse(response.data.fuzzyfication);
                const agregation = JSON.parse(response.data.agregation);
                const activation = JSON.parse(response.data.activation);
                const accumulated = JSON.parse(response.data.accumulated);
                const defuzzyficated = response.data.defuzzyficated
                this.setState({fuzzyfication, activation, agregation, accumulated, defuzzyficated});
                console.log(fuzzyfication, agregation, activation, accumulated);
            })
    }

    render() {
        const {formFields, fuzzyfication, activation, accumulated, defuzzyficated} = this.state;

        return (
            <article className={"form"}>
                <div className={"title"}>
                    <h1>Лабораторная работа № 3. Разработка ЭС с поддержкой нечёткого вывода</h1>
                    <h3>204-321 Забельский Андрей</h3>
                </div>
                <div className={"inputs"}>
                    {formFields.map(field => {
                        return (
                            <AntdSlider onChange={(id, value) => {
                                this.onSliderChange(id, value)
                            }} id={field?.id} max={field?.x_max} min={field?.x_min} label={field?.name}
                                        key={field?.name}/>
                        )
                    })}

                    <Button type="primary" onClick={() => {
                        this.send()
                    }}>Рассчитать</Button>
                </div>
                <div className={"graphs"}>
                    <div className={"fuzzyfication"}>
                        {Object.keys(fuzzyfication).map(key => {
                            return <div><FuzzyficationGraph fuzzyficationResults={fuzzyfication[key]} key={key}/></div>
                        })}
                    </div>
                    <div className={"activation"}>
                        {activation.graph && <ActivatedFunctionGraph activationResults={activation.graph}/>}
                        {activation.activated &&
                        <div className={"activated"}
                             style={{gridTemplateRows: `repeat(${activation.activated.length > 2 ? 2 : 1}, 1fr)`}}>
                            {activation.activated.map((graph) => {
                                return <ActivatedGraph {...graph} label={graph.name} key={graph.text}/>
                            })}
                        </div>
                        }
                    </div>
                    <div className={"accumulation"}>
                        {accumulated && activation.graph &&
                        <AccumulatedGraph baseGraph={activation.graph} graph={accumulated}/>}
                    </div>
                </div>

                <div className={"result"}>
                    {defuzzyficated && <h1>Серьезность неисправности примерно {Math.floor(defuzzyficated)}%</h1>}
                </div>
            </article>
        )
    }
}
