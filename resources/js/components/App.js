import React from "react";

import "./app.scss";
import 'antd/dist/antd.css';
import axios from "axios";
import {Form} from "./Form/Form";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
    }

    render() {
        const {} = this.state;
        return (
            <div className={"site-wrapper"}>
                <Form/>
            </div>
        )
    }
}

export default App;

