import React from 'react';
import { Slider } from 'antd';
import 'antd/dist/antd.css';
import './AntdSlider.scss';

export class AntdSlider extends React.Component {
    state = {
        value: 0,
    };

    handleChange = value => {
        this.setState({ value });
        this.props.onChange(this.props.id, value);
    };

    render() {
        const { max, min, label } = this.props;
        const { value } = this.state;
        return (
            <div className="icon-wrapper antd-slider">
                <span>{label}:</span>
                <Slider {...this.props} onChange={this.handleChange} value={value} min={min} max={max} step={50}/>
            </div>
        );
    }
}
