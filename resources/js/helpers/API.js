import axios from "axios/index";

export const API = (path, params) => {
    axios.post(`/${path}.json`, {_token: this.token, ...params})
        .then(data => {
            return data
        })
        .catch(error => {
            console.log(error.message);
        });
}
