<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>204-321 Забельский Лабораторная работа №3</title>

    </head>
    <body>
       <div id="root">

       </div>
       <div id="sprite" class="sprite"></div>

       <div id="root"></div>
       <script src="/assets/js/app.js"></script>
       <script>
           (function (t) {
               var r = new XMLHttpRequest();
               r.open('GET', '/assets/svg/sprite.svg', true);
               r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
               r.onreadystatechange = function () {
                   if (r.readyState !== 4) {
                       return void 0;
                   }
                   var div = document.createElement('DIV');
                   div.id = 'LZ';
                   div.className = 'LZ';
                   div.innerHTML = r.responseText;
                   t.appendChild(div);
               };
               r.send();
           })(document.body);
       </script>
    </body>
</html>
